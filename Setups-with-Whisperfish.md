Whisperfish can be configured as either a *primary device* or a *secondary device*, a.k.a. *linked device*.  Whisperfish will ask you which configuration you want during the registration phase. If you want to switch the mode, you will have to [reset Whisperfish](Frequently-Asked-Questions#how-do-i-reset-whisperfish-to-be-able-to-register-again).  The mode of registration does not influence the supported feature set.  You may have as many linked devices as your heart desires: multiple Whisperfish clients, multiple Flare's, multiple Molly's and Signal Desktop.

Depending on the mode of registration, different setups are possible. If you are new to Whisperfish, we recommend using an official app (Signal Android or Signal iOS) as primary device, and registering Whisperfish as secondary device (see next section).

# Whisperfish as *secondary/linked device* (recommended)

A registration as *secondary* device will work like Signal Desktop:
your Signal-Android or Signal-iOS registration remains valid,
and Whisperfish is merely a puppet that also receives and sends messages in your primary's name.
This ensures you can always fall back to the official Signal app.

In order to use a secondary registration, you will need to transfer the generated QR code to Signal-Android.
You may do this through photographing or printing or screenshotting.

## Signal Android or Signal iOS as main registration (recommended)

When an official app is your primary registration, you always have a working back-up application, in case Whisperfish fails.

Install Signal Android or Signal iOS as you per Signal's instructions. You may do this on the same phone as you will install Whisperfish on.
Afterwards, install Whisperfish, and register as secondary/linked device.
There is a time limit on the linking phase, so make sure to complete the linking as fast as you can.
If your Android installation is on the same device, you might need some trickery with screenshots or screensharing to get the QR code scanned.

## Molly as main registration

Alternatively, you can use [Molly](https://molly.im/) as your primary client, and use Whisperfish as secondary client. Molly is a hardened Signal Android fork with some amazing extra features from a reputable developer.

# Whisperfish as *primary device*

If you have Whisperfish registered as your primary device, you can link several other clients to Whisperfish.
**Importantly, Signal Android and Signal iOS do not support being a secondary device**.
The only official Signal apps that register as secondary, are Signal iPad and Signal Desktop.

There exist multiple unofficial applications that could link to Whisperfish, although not all are known to work.
A checkmark next to the application means it has been tested and known to work:

- [X] [Signal Desktop](https://signal.org/download/), the official Electron-based desktop client
- [ ] [Molly](https://molly.im/), a hardened Signal Android fork
- [ ] [Beeper](https://www.beeper.com/), "All your chats in one app", but [please don't use this](http://rubdos.be/privacy/signal/2023/09/07/my-problem-with-beeper.html)
- [ ] [Flare](https://gitlab.com/Schmiddiii/flare/), a GTK-based desktop client from the Whisperfish family

## Molly as *secondary/linked* device

Molly is a hardened Signal Android fork with some amazing extra features from a reputable developer.
Since recently, Molly can be configured as secondary/linked device.
This is for example useful for people who want to use Signal on an Android tablet, or for existing Whisperfish users who want a stable, Android-based back-up in case Whisperfish fails on them.
