Since the build-system rework, there was a [a conflict between libmozembed and libsqlcipher](https://users.rust-lang.org/t/c-library-symbol-conflicts-hiding-symbols-of-static-library-cc/73389), practically making the registration procedure crash on SailfishOS. Since beta.8, there is a workaround, although it is rather involved. This was [mostly fixed](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/274 "Add the main QML for the reCaptcha QML-only application") in beta.10, and [finalized](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/284 "Migrate config.yml and storage folders to new location") in beta.11.

This method is still required with devices on older Sailfish OS versions. For example, Jolla Phone will remain on Sailfish 3.4.0, and while it can open the captcha page in Browser, the embedded browser component simply crashes.

## Requirements

- A computer with a browser.
- `ssh` access to your phone. Read about [developer mode on Jolla's website](https://jolla.zendesk.com/hc/en-us/articles/202011863-How-to-enable-Developer-Mode).

## Procedure

Note: make sure that you have read the instructions completely before starting. You can try this multiple times, but there is a time limit that the captcha code is valid. I estimate that, from the moment you file the captcha to the moment you enter and confirm your phone number, you have less than a minute to complete the procedure.

1. Close Whisperfish on your phone, if it is already started.
2. Open the browser on your computer on https://signalcaptchas.org/registration/generate.html
3. Open the browser console. In Firefox, you can use `Control`+`Shift`+`K` to open it. In Firefox, you need to watch the _console_. In Chrome or Chromium, you need to watch the _networks tab_. Please refer to the screenshots below.
4. There will be some warnings already visible, don't worry about these.
5. Only now file the captcha that is presented on the webpage.
6. When the captcha is filed, you get a warning in the console, in form of "Prevented navigation to ..", or a failed request in the network tab in Chrome. Part of this warning is a very long URL, starting with `signalcaptcha://`. Copy the URL to your clipboard, excluding the quotation marks.
7. Via SSH, run `harbour-whisperfish --verbose --captcha '<THE URL HERE>'`, and complete the registration on your phone. The easiest way is to run this as a single command, i.e. `ssh nemo@\[target\] harbour-whisperfish --verbose --captcha '<THE URL HERE>'`.
8. When the registration is finished, close Whisperfish, then close SSH.

![Filed captcha in Firefox](captcha-workaround-firefox.png)

![Filed captcha in Chromium](captcha-workaround-chromium.png)