# The encrypted Database file

> **Warning:** Always close Whisperfish before editing the database!

> **Warning:** Remember to make (and practice with) backups first!

The database file `harbour-whisperfish.db` located in `~/.local/share/harbour-whisperfish/db` is an encrypted SQLite database, using [SQLCipher](https://github.com/sqlcipher/sqlcipher).

## Database encryption key
To access the data stored in this database, you need both your personal whisperfish password and the 8 byte `salt` data that is contained in the `~/.local/share/harbour-whisperfish/db` directory.
Together, they give rise to a 32 byte hash value obtained using the [`scrypt` algorithm](https://en.wikipedia.org/wiki/Scrypt), which is then used as the encryption key for the database.

Whisperfish uses default values to generate the scrypt hash, i.e. `N = 16384 = 2^14`, `r = 8`, and `p = 1`.


The easiest way to generate the hexadecimal key for use with sqlcipher is using the `whisperfish-storage-key`. It currently comes bundled in the beta releases, but this may change in the future.

Run the command with your password to get the hexadecimal key:

```
$ cd ~/.local/share/be.rubdos/harbour-whisperfish/db/
$ ./whisperfish-storage-key P455w0rd
Database key: "45c22d03ecbeb7df9c8f33336902f8135628d36c87e4f28e30a374cb6a42179d"
```

The key would then be used like so:

```
sqlcipher harbour-whisperfish.db
sqlite> PRAGMA key = "x'45c22d03ecbeb7df9c8f33336902f8135628d36c87e4f28e30a374cb6a42179d'";
sqlite> PRAGMA cipher_page_size = 4096;
sqlite> PRAGMA cipher_hmac_algorithm = HMAC_SHA1;
sqlite> PRAGMA cipher_kdf_algorithm = PBKDF2_HMAC_SHA1;
sqlite> PRAGMA kdf_iter = 64000;
```

Note that `whisperfish-storage-key` does not check that the password entered or the key generated is correct. You can run a simple query first to make sure the key and the parameters are correct. If they are not, an error occurs:

```
sqlite> SELECT COUNT(*) FROM MESSAGES;
Error: file is not a database
```

Should the binary be missing, you can compile them locally as well:

```sh
sfdk build --with tools
```

### Python code snippet to generate the key
Another way to generate the encryption key is to use one of the two Python packages `hashlib` or `scrypt`:

#### `hashlib`:
```python
import hashlib
import os

password = b'<YOURPASSWORD>'
saltfile = os.path.expanduser("~/.local/share/be.rubdos/harbour-whisperfish/db/salt")

with open(saltfile, "rb") as f:
    salt = f.read()
    print(hashlib.scrypt(pwb, salt=salt, n=1 << 14, r=8, p=1, maxmem=0, dklen=32).hex())
```
#### `scrypt`:
```python
import scrypt
import os

password = b'<YOURPASSWORD>'
saltfile = os.path.expanduser("~/.local/share/be.rubdos/harbour-whisperfish/db/salt")

with open(saltfile, "rb") as f:
    salt = f.read()
    print(scrypt.hash(pwb, salt, buflen = 32).hex())
```

## Database access
You can now access your data with [SQLCipher](https://www.zetetic.net/sqlcipher/) (command line tool), [DB Browser for SQLite](https://sqlitebrowser.org/) (AppImage has built-in SQLCipher support), [DBeaver](https://dbeaver.io/) (SQLCipher support needs to be [installed](https://gist.github.com/thecodingcod/a3987ddde8892d1a6bda8bd94febb7d0) first), or any other SQLcipher-compatible tool.

The Whisperfish database for the most part uses the default `sqlcipher v3` format, but the page size is set to 4096.
Please note, that, at the time of testing (SQLCipher 4.4.3 community), also the [KDF algorithm](https://www.zetetic.net/sqlcipher/sqlcipher-api/#cipher_kdf_algorithm) has to be set to PBKDF2_HMAC_SHA1, even though we will already provide a raw key.

Settings are:
- page size: 4096
- KDF iterations: 64000
- HMAC algorithm: SHA1
- KDF algorithm: SHA1
- plaintext header size: 0

Finally, you only need to use the hexadecimal hash value as generated above and set the corresponding cipher parameters.

For instance, `sqlcipher` requires one to use the format `"x'<hex key>'"` to indicate that the key is already in hexadecimal format and must not be salted any further, see also [here](https://www.zetetic.net/sqlcipher/sqlcipher-api/#key).
In `sqlitebrowser` and `DB Browser for SQLite`, the key has to be entered in *raw* format and prepended by a `0x`.

## Exporting an SQLite database file

Operating on an encrypted database file can be cumbersome. You can export your database as a plain SQLite database which has no encryption:

```
$ sqlcipher ~/.local/shared/harbour-whisperfish/db/harbour-whisperfish.db
sqlite> PRAGMA key = "x'1234...'";
sqlite> PRAGMA cipher_page_size = 4096;
sqlite> PRAGMA cipher_hmac_algorithm = HMAC_SHA1;
sqlite> PRAGMA cipher_kdf_algorithm = PBKDF2_HMAC_SHA1;
sqlite> PRAGMA kdf_iter = 64000;
sqlite> ATTACH DATABASE 'plaintext.db' AS plaintext KEY '';  -- empty key will disable encryption
sqlite> SELECT sqlcipher_export('plaintext');
sqlite> DETACH DATABASE plaintext;
```

You can then use (a recent enough) SQLite or whichever SQLite-aware database tool you prefer without having to worry about SQLCipher support and entering the encryption parameters.
